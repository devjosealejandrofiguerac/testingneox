import { Test, TestingModule } from '@nestjs/testing';
import { SolicitudController } from './solicitud.controller';
import { SolicitudService } from './solicitud.service';
import { CreateSolicitudDto } from './dto/create-solicitud.dto';
import { UpdateSolicitudDto } from './dto/update-solicitud.dto';
import { of } from 'rxjs';
import { SolicitudServiceMock } from './solicitud-service-mock';
import { Solicitud } from './entities/solicitud.entity';

describe('SolicitudController', () => {
  let controller: SolicitudController;
  let service: SolicitudService; 
  const mockSolicitud: Solicitud = { 
    id: 1,
    nombre:'Jose Figuera',
    cargo:'Angular Developer',
    unidad:'Departamento Informatica',
    telefono: '+56972061555',
    email:'josealejandrofiguerac@gmail.com',
    tipo:'',
    nombreActividad:'DevOps',
    start: undefined,
    end:undefined,
    dia:'',
    horaInicio:'',
    horaFin:'',
  };

  beforeEach(async () => {
    const SolicitudServiceProvider = { 
      provide: SolicitudService,
      useClass: SolicitudServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [SolicitudController],
      providers: [SolicitudService, SolicitudServiceProvider], 
    })
      .overrideProvider(SolicitudService)
      .useClass(SolicitudServiceMock) 
      .compile();

    controller = module.get<SolicitudController>(SolicitudController);
    service = module.get<SolicitudService>(SolicitudService); 
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('Deberia crear una Solicitud', async () => {
   const createSolicitudDto: CreateSolicitudDto = {
     nombre:'Jose Figuera',
     cargo:'Developer',
     unidad:'Departamento Informatica',
     telefono: '+56972061555',
     email:'josealejandrofiguerac@gmail.com',
     tipo:'',
     nombreActividad:'DevOps',
     start: undefined,
     end:undefined,
     dia:'',
     horaInicio:'',
     horaFin:'',
   };

    expect(await controller.create(createSolicitudDto)).toEqual({ 
      id: expect.any(Number),
      ...createSolicitudDto,
    });
  });

  it('Deberia Actualizar una Solicitud', async () => {
    const updateSolicitudDto: UpdateSolicitudDto = {
      nombre:'Jose Figuera Caraballo',
      cargo:'Angular Developer',
      unidad:'Departamento Informatica',
      telefono: '+56972061555',
      email:'josealejandrofiguerac@gmail.com',
      tipo:'',
      nombreActividad:'DevOps',
      start: undefined,
      end:undefined,
      dia:'',
      horaInicio:'',
      horaFin:'',
     };
    const solicitudId = 2;

    expect(await controller.update(solicitudId.toString(), updateSolicitudDto)).toEqual({ 
      id: solicitudId,
      ...updateSolicitudDto,
    });

 //Espiar el método update del objeto service. Ahora updateSpy monitoriza cada una de las llamadas que se hagan al método update del objeto service
    const updateSpy = jest.spyOn(service, 'update'); 
    controller.update(solicitudId.toString(), updateSolicitudDto); 

 // toHaveBeenCalledWith evalua los argumentos con los que se hacen las llamadas
    expect(updateSpy).toHaveBeenCalledWith(solicitudId, updateSolicitudDto); 
  });

  it('Deberia ubicar una solicitud', async () => { 
    const solicitudId = 2;
    expect(await (await controller.findOne(solicitudId.toString())).id).toEqual(
      solicitudId,
    );
  });

  it('Deberia ubicar todas las solicitudes', async () => { 
    expect(await controller.findAll()).toEqual([mockSolicitud]);
  });

  it('Deberia Eliminar una solicitud', async () => { 
    expect((await controller.remove('2'))['affected']).toEqual(1);
  });
});