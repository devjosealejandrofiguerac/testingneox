import { Solicitud } from "./entities/solicitud.entity";

export class SolicitudRepositoryMock {
    mockSolicitud: Solicitud = { 
      id: 1,
      nombre:'Jose Figuera',
      cargo:'Developer',
      unidad:'Departamento Informatica',
      telefono: '+56972061555',
      email:'josealejandrofiguerac@gmail.com',
      tipo:'',
      nombreActividad:'DevOps',
      start: undefined,
      end:undefined,
      dia:'',
      horaInicio:'',
      horaFin:'',
    };
  
    save(solicitud: Solicitud): Promise<Solicitud> {
      return Promise.resolve(this.mockSolicitud); 
    }
  
    findOne(id: number): Promise<Solicitud> {
      return Promise.resolve(this.mockSolicitud); 
    }
  
    find(): Promise<Solicitud[]> { 
      return Promise.resolve([this.mockSolicitud]);
    }
  
    delete(id: number): Promise<any> { 
      return Promise.resolve();
    }
  }