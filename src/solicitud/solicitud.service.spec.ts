import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Solicitud } from './entities/solicitud.entity';
import { SolicitudService } from './solicitud.service';
import { CreateSolicitudDto } from './dto/create-solicitud.dto';
import { UpdateSolicitudDto } from './dto/update-solicitud.dto';
import { SolicitudRepositoryMock } from './solicitud-repository-mock';


describe('SolicitudService', () => {
  let service: SolicitudService;
  const mockSolicitud: Solicitud = { 
    id: 1,
    nombre:'Jose Figuera',
    cargo:'Developer',
    unidad:'Departamento Informatica',
    telefono: '+56972061555',
    email:'josealejandrofiguerac@gmail.com',
    tipo:'',
    nombreActividad:'DevOps',
    start: undefined,
    end:undefined,
    dia:'',
    horaInicio:'',
    horaFin:'',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SolicitudService,
        { 
          provide: getRepositoryToken(Solicitud), 
          useClass: SolicitudRepositoryMock, 
        },
      ],
    }).compile();

    service = module.get<SolicitudService>(SolicitudService);
  });


  it('should be defined', () => {
    expect(service).toBeDefined();
  });


  it('Deberia crear una solicitud', async () => {
    const createSolicitudDto: CreateSolicitudDto = { 
      nombre:'Jose Figuera',
      cargo:'Developer',
      unidad:'Departamento Informatica',
      telefono: '+56972061555',
      email:'josealejandrofiguerac@gmail.com',
      tipo:'',
      nombreActividad:'DevOps',
      start: undefined,
      end:undefined,
      dia:'',
      horaInicio:'',
      horaFin:'',
    };

    expect(await service.create(createSolicitudDto)).toEqual({ 
      id: expect.any(Number),
      ...createSolicitudDto,
    });
  });



  it('Deberia Actualizar una Solicitud', async () => {
    const updateSolicitudDto: UpdateSolicitudDto = {
      nombre:'Jose Figuera Caraballo',
      cargo:'Developer',
      unidad:'Departamento Informatica',
      telefono: '+56972061555',
      email:'josealejandrofiguerac@gmail.com',
      tipo:'',
      nombreActividad:'DevOps',
      start: undefined,
      end:undefined,
      dia:'',
      horaInicio:'',
      horaFin:'',
     };
    const solicitudId = 1;

  
    expect(await service.update(solicitudId, updateSolicitudDto)).toEqual({ 
      id: solicitudId,
      ...updateSolicitudDto,
    });

  });



  
  it('Deberia ubicar una solicitud', async () => { 
    const solicitudId = 1;
    expect(await (await service.findOne(solicitudId)).id).toEqual(solicitudId);
  });

  it('Deberia ubicar todas las solicitudes', async () => { 
    expect(await service.findAll()).toEqual([mockSolicitud]);
  });

  it('deberia eliminar una solicitud', async () => { 
    expect(await service.remove(1)).toBeDefined;
  });


});
